# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
**DSCADA Operating Panel is used by operators and field technicians to view, operate, and test DSCADA devices through the Hexagon Inservice and Siemens Spectrum systems.**
* Version (usually follows the version of Inservice followed by a panel version)
**9.3.001**
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
**none**
* Dependencies
**Any workstation running the Hexagon IDispatcher application.**
* Database configuration
* How to run tests
* Deployment instructions
**Copy executable and config to Intergraph\IDISP\bin (typically located in C:\Program Files (x86))**

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
**matthew.eby@oncor.com**
* Other community or team contact
**DMSSupport@oncor.com**